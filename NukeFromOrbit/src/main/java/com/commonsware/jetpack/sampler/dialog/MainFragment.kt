/*
  Copyright (c) 2020-2021 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Jetpack_

  https://commonsware.com/Jetpack
*/

package com.commonsware.jetpack.sampler.dialog

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.NavHostFragment
import com.commonsware.jetpack.sampler.dialog.databinding.FragmentMainBinding
import com.commonsware.jetpack.sampler.util.ViewBindingFragment
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class MainFragment :
  ViewBindingFragment<FragmentMainBinding>(FragmentMainBinding::inflate) {
  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    requireBinding().nuke.setOnClickListener {
      NavHostFragment.findNavController(this@MainFragment)
        .navigate(R.id.confirmNuke)
    }

    val navController = NavHostFragment.findNavController(this)
    val viewModelProvider =
      ViewModelProvider(navController.getViewModelStoreOwner(R.id.nav_graph))
    val vm = viewModelProvider[GraphViewModel::class.java]

    viewLifecycleOwner.lifecycleScope.launch {
      viewLifecycleOwner.lifecycle.repeatOnLifecycle(Lifecycle.State.STARTED) {
        vm.resultStream.collect { wasAccepted ->
          if (wasAccepted) {
            Toast.makeText(requireContext(), "BOOOOOOOM!", Toast.LENGTH_LONG)
              .show()
          }
        }
      }
    }
  }
}