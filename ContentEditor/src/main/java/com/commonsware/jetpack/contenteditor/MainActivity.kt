/*
  Copyright (c) 2019-2021 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Jetpack_

  https://commonsware.com/Jetpack
*/

package com.commonsware.jetpack.contenteditor

import android.Manifest
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.text.TextUtils
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import com.commonsware.jetpack.contenteditor.databinding.ActivityMainBinding
import java.io.File

private const val FILENAME = "test.txt"

class MainActivity : AppCompatActivity() {
  private val motor: MainMotor by viewModels()
  private var current: Uri? = null
  private lateinit var binding: ActivityMainBinding

  private val openDoc =
    registerForActivityResult(ActivityResultContracts.OpenDocument()) { uri ->
      binding.text.setText("")
      uri?.let { motor.read(it) }
    }

  private val createDoc =
    registerForActivityResult(ActivityResultContracts.CreateDocument()) { uri ->
      binding.text.setText("")
      uri?.let { motor.read(it) }
    }

  private val requestPerm = registerForActivityResult(ActivityResultContracts.RequestPermission()) {
    if (it) {
      loadFromDir(Environment.getExternalStorageDirectory())
    } else {
      Toast.makeText(this, R.string.msg_sorry, Toast.LENGTH_LONG).show()
    }
  }

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)

    binding = ActivityMainBinding.inflate(layoutInflater)

    setContentView(binding.root)

    motor.results.observe(this) { result ->
      when (result) {
        StreamResult.Loading -> {
          binding.progress.visibility = View.VISIBLE
          binding.text.isEnabled = false
        }
        is StreamResult.Content -> {
          binding.progress.visibility = View.GONE
          binding.text.isEnabled = true
          current = result.source
          binding.title.text = result.source.toString()

          if (TextUtils.isEmpty(binding.text.text)) {
            binding.text.setText(result.text)
          }
        }
        is StreamResult.Error -> {
          binding.progress.visibility = View.GONE
          binding.text.setText(result.throwable.localizedMessage)
          binding.text.isEnabled = false
          Log.e("ContentEditor", "Exception in I/O", result.throwable)
        }
      }
    }

    loadFromDir(filesDir)
  }

  override fun onCreateOptionsMenu(menu: Menu): Boolean {
    menuInflater.inflate(R.menu.actions, menu)

    return super.onCreateOptionsMenu(menu)
  }

  override fun onOptionsItemSelected(item: MenuItem): Boolean {
    when (item.itemId) {
      R.id.loadInternal -> {
        loadFromDir(filesDir)
        return true
      }

      R.id.loadExternal -> {
        loadFromDir(getExternalFilesDir(null))
        return true
      }

      R.id.loadExternalRoot -> {
        requestPerm.launch(Manifest.permission.WRITE_EXTERNAL_STORAGE)
        return true
      }

      R.id.openDoc -> {
        openDoc.launch(arrayOf("text/*"))
        return true
      }

      R.id.newDoc -> {
        createDoc.launch(FILENAME)
        return true
      }

      R.id.save -> {
        current?.let { motor.write(it, binding.text.text.toString()) }
        return true
      }
    }

    return super.onOptionsItemSelected(item)
  }

  private fun loadFromDir(dir: File?) {
    binding.text.setText("")
    motor.read(Uri.fromFile(File(dir, FILENAME)))
  }
}
