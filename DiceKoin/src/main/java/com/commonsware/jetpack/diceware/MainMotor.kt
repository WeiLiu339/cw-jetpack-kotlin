/*
  Copyright (c) 2019-2020 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Jetpack_

  https://commonsware.com/Jetpack
*/

package com.commonsware.jetpack.diceware

import android.net.Uri
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

private const val DEFAULT_WORD_COUNT = 6

class MainMotor(private val repo: PassphraseRepository) : ViewModel() {
  private val _results = MutableLiveData<MainViewState>()
  val results: LiveData<MainViewState> = _results
  private var wordsDoc = ASSET_URI

  init {
    generatePassphrase(DEFAULT_WORD_COUNT)
  }

  fun generatePassphrase() {
    generatePassphrase(
      (results.value as? MainViewState.Content)?.wordCount ?: DEFAULT_WORD_COUNT
    )
  }

  fun generatePassphrase(wordCount: Int) {
    _results.value = MainViewState.Loading

    viewModelScope.launch(Dispatchers.Main) {
      _results.value = try {
        val randomWords = repo.generate(wordsDoc, wordCount)

        MainViewState.Content(randomWords.joinToString(" "), wordCount)
      } catch (t: Throwable) {
        MainViewState.Error(t)
      }
    }
  }

  fun generatePassphrase(wordsDoc: Uri) {
    this.wordsDoc = wordsDoc

    generatePassphrase()
  }
}
