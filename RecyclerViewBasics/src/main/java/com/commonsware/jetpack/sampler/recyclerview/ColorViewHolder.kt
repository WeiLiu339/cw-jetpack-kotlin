/*
  Copyright (c) 2018-2020 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Jetpack_

  https://commonsware.com/Jetpack
*/

package com.commonsware.jetpack.sampler.recyclerview

import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.commonsware.jetpack.sampler.recyclerview.databinding.RowBinding

class ColorViewHolder(private val binding: RowBinding) :
  RecyclerView.ViewHolder(binding.root) {
  init {
    binding.root.setOnClickListener { _ ->
      Toast.makeText(
        binding.label.context,
        binding.label.text,
        Toast.LENGTH_LONG
      ).show()
    }
  }

  fun bindTo(color: Int) {
    binding.label.text =
      binding.label.context.getString(R.string.label_template, color)
    binding.swatch.setBackgroundColor(color)
  }
}
